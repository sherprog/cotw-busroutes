var district = {
  lat: 44.789618927670915,
  long: -106.95500000000000
};

var mapOptions = {
  zoom: 12,
  center: new google.maps.LatLng(district.lat,district.long)
};

var map = new google.maps.Map(document.getElementById('map'),mapOptions);

function Marker(title,latitude,longitude) {
  return new google.maps.Marker({
      position: new google.maps.LatLng(latitude,longitude),
      map: map,
      title: title
  });
}

var item = new Marker('Test',district.lat,district.long);
